/**
 * This file contains implementations for methods in the PageTable class.
 *
 * You'll need to add code here to make the corresponding tests pass.
 */

#include "page_table/page_table.h"

using namespace std;


size_t PageTable::get_present_page_count() const {
    int num_present = 0;

    for (auto row : rows){
        if(row.present) num_present++;
    }

    return num_present;
}


size_t PageTable::get_oldest_page() const {
    // <Oldest time, Oldest Frame>
    pair<int,int> oldest(INT_MAX, 0);

    int i = 0;
    for (auto row : rows){
        if (row.present && row.loaded_at < oldest.first){
            oldest.first = row.loaded_at;
            oldest.second = i;
        }
        i++;
    }

    return oldest.second;
}


size_t PageTable::get_least_recently_used_page() const {
    // <Last time, Frame>
    pair<int,int> stalest(INT_MAX, 0);

    int i = 0;
    for (auto row : rows){
        if (row.present && row.last_accessed_at < stalest.first){
            stalest.first = row.last_accessed_at;
            stalest.second = i;
        }
        i++;
    }

    return stalest.second;
}
