/**
 * This file contains implementations for methods in the VirtualAddress class.
 *
 * You'll need to add code here to make the corresponding tests pass.
 */

#include "virtual_address/virtual_address.h"

using namespace std;


VirtualAddress VirtualAddress::from_string(int process_id, string address) {
    int page = bitset<10>(address.substr(0,PAGE_BITS)).to_ulong();
    int offset = bitset<6>(address.substr(PAGE_BITS, OFFSET_BITS)).to_ulong();

    return VirtualAddress(process_id, page, offset);
}


string VirtualAddress::to_string() const {
    string string_formatted;
    string_formatted += bitset<PAGE_BITS>(page).to_string();
    string_formatted += bitset<OFFSET_BITS>(offset).to_string();

    return string_formatted;
}


ostream& operator <<(ostream& out, const VirtualAddress& address) {
    out
        << "PID " << address.process_id
        << " @ " << address.to_string()
        << " [page: " << address.page
        << "; offset: " << address.offset
        << "]";
    return out;
}
