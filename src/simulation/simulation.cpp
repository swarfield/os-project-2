/**
 * This file contains implementations for the methods defined in the Simulation
 * class.
 *
 * You'll probably spend a lot of your time here.
 */

#include "simulation/simulation.h"

using namespace std;
using boost::format;


void Simulation::run(FlagOptions options) {
    // Parse options
    parse_flags(options);

    // Initialize free frames
    for (int i = 0; i< NUM_FRAMES; i++) {
        free_frames.push_back(all_frames + i);
    }



    // Run Sim Memory access from file
    int pid;
    string virt_addr_strg;

    while(input_file >> pid >> virt_addr_strg){
        VirtualAddress addr = VirtualAddress::from_string(pid, virt_addr_strg);

        if (verbose) {
            cout << addr << endl;
        }

        perform_memory_access(addr);
        time++;
    }

    print_summary();
}


char Simulation::perform_memory_access(const VirtualAddress& address) {

    PhysicalAddress* pointy_boi;
    bool page_fault = false;
    Process* cur_process = processes.at(address.process_id);

    // Find the page and get it an address
    if (cur_process->is_valid_page(address.page)) {
        if (!cur_process->pages.at(address.page)->is_valid_offset(address.offset)){
            cout << "SEGFAULT - INVALID OFFSET\n";
            exit(-1);

        }
        total_accesses++;

        PageTable::Row* pt_entry 
            = &(cur_process->page_table.rows[address.page]);

        cur_process->memory_accesses++;

        // If in memory
        if (pt_entry->present){
            pointy_boi = new PhysicalAddress(pt_entry->frame, address.offset);
        } else{
            page_fault = true;
            handle_page_fault(cur_process, address.page);
            pointy_boi = new PhysicalAddress(pt_entry->frame, address.offset);
        }

        pt_entry->last_accessed_at = time;

    } else {
        cout << "SEGFAULT - INVALID PAGE\n";
        exit(-1);
    }

    // Get the physical addr
    if (verbose){
        if (page_fault){
            cout << "    -> PAGE FAULT\n";
        } else {
            cout << "    -> IN MEMORY\n";
        }

        cout << "    -> physical address " << *pointy_boi << endl
             << "    -> RSS: " 
             << cur_process->get_rss() 
             << endl << endl;
    }

    delete pointy_boi;
    return 0;
}


void Simulation::handle_page_fault(Process* process, size_t page) {
    // TODO: implement me
    page_faults++;
    process->page_faults++;
    int frame_number;
    
    // if the max amount of frames has not been reached
    if (process->get_rss() < MAX_FRAMES_PER_PROC){
        // Get a new frame
        Frame* frame = free_frames.front();
        free_frames.erase(free_frames.begin());
        frame->set_page(process, page);
        frame_number = next_open_frame;
        next_open_frame++;

        // update pg_table
        // Update the page table entry
        load_page(process, page, frame_number);

        return;

    } else { // else replace one of the current frame contense 
        if(strategy == ReplacementStrategy::FIFO){
            // get the oldest pg and unload it
            int pg_to_unload = process->page_table.get_oldest_page();
            frame_number = unload_page(process, pg_to_unload);
            load_page(process, page, frame_number);

        } else { // LRU
            int pg_to_unload = process->page_table.get_least_recently_used_page();
            frame_number = unload_page(process, pg_to_unload);
            load_page(process, page, frame_number);

        }
    }

}

int Simulation::unload_page(Process* process, size_t page){\
    PageTable::Row* pt_entry 
        = &(process->page_table.rows[page]);

    pt_entry->present = false;
    return pt_entry->frame;
}

void Simulation::load_page(Process* process, size_t page, size_t frame_number){\
    PageTable::Row* pt_entry 
        = &(process->page_table.rows[page]);

    pt_entry->present = true;
    pt_entry->frame = frame_number;
    pt_entry->loaded_at = time;
}


void Simulation::print_summary() {
    if (!csv) {
        format process_fmt(
            "Process %3d:  "
            "ACCESSES: %-6lu "
            "FAULTS: %-6lu "
            "FAULT RATE: %-8.2f "
            "RSS: %-6lu\n");

        for (auto entry : processes) {
            cout << process_fmt
                % entry.first
                % entry.second->memory_accesses
                % entry.second->page_faults
                % entry.second->get_fault_percent()
                % entry.second->get_rss();
        }

         // Print statistics.
        format summary_fmt(
            "\n%-25s %12lu\n"
            "%-25s %12lu\n"
            "%-25s %12lu\n");

        cout << summary_fmt
            % "Total memory accesses:"
            % total_accesses
            % "Total page faults:"
            % page_faults
            % "Free frames remaining:"
            % free_frames.size();
     }

    if (csv) {
        format process_fmt(
            "%d,"
            "%lu,"
            "%lu,"
            "%.2f,"
            "%lu\n");

        for (auto entry : processes) {
            cout << process_fmt
                % entry.first
                % entry.second->memory_accesses
                % entry.second->page_faults
                % entry.second->get_fault_percent()
                % entry.second->get_rss();
        }

         // Print statistics.
        format summary_fmt(
            "%lu,,,,\n"
            "%lu,,,,\n"
            "%lu,,,,\n");

        cout << summary_fmt
            % total_accesses
            % page_faults
            % free_frames.size();
    }
}

void Simulation::parse_flags(FlagOptions opts) {
    // Open and Read File
    input_file.open(opts.filename);
    input_file >> num_processes;

    // Load The Processes and their data
    for (int i = 0; i < num_processes; i++){
        int pid;
        string proc_data_file;

        input_file >> pid >> proc_data_file;

        ifstream in (proc_data_file);
        Process* a_boi = Process::read_from_input(in);

        processes.emplace(pid, a_boi);

        //cout << "Process: " << pid << " Size: " << a_boi->size() << endl;
    }

    // Set Sim Vars
    csv = opts.csv;
    MAX_FRAMES_PER_PROC = opts.max_frames;
    verbose = opts.verbose;
    strategy = opts.strategy;
}
