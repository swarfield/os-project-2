#!/bin/bash
# Author: Alexandra Joseph, Samuel Warfield

solution_outputs_dir="verify/outputs"
verify_part="$1"

if [ "$verify_part" == "part_1" ]; then
    touch your_soln
    
    make clean && make
    ./mem-sim inputs/sim_1 > your_soln
    diff -sw "$solution_outputs_dir"/deliverable_01/sim_1_output your_soln

    rm your_soln

elif [ "$verify_part" == "part_2" ]; then

    make clean && make
    printf "\nTesting FIFO\n\n"

    printf "Checking no flag output\n"
    ./mem-sim inputs/sim_1 > your_soln
    diff -sw "$solution_outputs_dir"/deliverable_02/sim_1_output_no_flag your_soln
    printf "\n"

    printf "Checking verbose flag output\n"
    ./mem-sim -v inputs/sim_1 > your_soln
    diff -sw "$solution_outputs_dir"/deliverable_02/sim_1_output_v_flag your_soln
    printf "\n"

    printf "Checking csv flag output\n"
    ./mem-sim -c inputs/sim_1 > your_soln
    diff -s "$solution_outputs_dir"/deliverable_02/sim_1_output_c_flag your_soln
    printf "\n"

    printf "\nTesting LRU / Segfault\n\n"

    printf "Checking no flag output\n"
    ./mem-sim inputs/sim_segfault_1 > your_soln
    diff -sw "$solution_outputs_dir"/sim_segfault_1_output_no_flag your_soln
    printf "\n"
    ./mem-sim inputs/sim_segfault_2 > your_soln
    diff -sw "$solution_outputs_dir"/sim_segfault_2_output_no_flag your_soln
    printf "\n"


    printf "Checking verbose flag output\n"
    ./mem-sim -vs LRU inputs/sim_segfault_1 > your_soln
    diff -sw "$solution_outputs_dir"/sim_segfault_1_output_lru_v_flag your_soln
    printf "\n"

    ./mem-sim -vs LRU inputs/sim_segfault_2 > your_soln
    diff -sw "$solution_outputs_dir"/sim_segfault_2_output_lru_v_flag your_soln
    printf "\n"

    rm your_soln

else
    printf "Input part_1 or part_2 after ./verify_output_format.sh.\n"

    printf "Like this: ./verify_output_format.sh part_1 or ./verify_output_format.sh part_2"
fi
